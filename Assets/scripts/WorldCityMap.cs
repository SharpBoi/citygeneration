﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldCityMap : MonoBehaviour {

    #region FIELDS
    [Header("Map settings")]
    public int MyChunkSize = 5;
    public int MyLinesCntPerChunkSide = 2;
    public int MyChunkScale = 2;

    [Header("Generation target")]
    public List<Transform> MyTargetsGenTo;
    public int MyGenBoundSize = 1;

    [Header("Tiles")]
    public WorldTile[] MyRoads;
    public WorldTile[] MyGrounds;
    public WorldTile[] MyBuildings;


    private List<WorldCityChunk> chunks = new List<WorldCityChunk>();
    #endregion

    #region PROPS
    public int MyTrueChunkSize { get { return MyChunkSize * MyChunkScale; } }


    #endregion

    #region FUNCS
    private WorldCityChunk myCreateChunk() {
        GameObject go = new GameObject();
        go.transform.SetParent(transform);
        go.name = "chunk (" + chunks.Count + ")";
        WorldCityChunk chunk = go.AddComponent<WorldCityChunk>();

        return chunk;
    }
    public void MyGenerate() {

    }

    private WorldCityChunk myGetChunkAtPos(int x, int z) {
        for (int i = 0; i < chunks.Count; i++) {
            if (chunks[i].MyMapPos.x == x && chunks[i].MyMapPos.y == z)
                return chunks[i];
        }

        return null;
    }
    private bool myHasChunkAtPos(int x, int z) {
        if (myGetChunkAtPos(x, z) == null)
            return false;

        return true;
    }
    #endregion

    #region UNITY_FUNCS
    private void Start() {
        
    }

    private void FixedUpdate() {

        for (int i = 0; i < MyTargetsGenTo.Count; i++) {

            // calc target norm pos on map
            Vector2Int tgtNormPos = Vector2Int.zero;
            tgtNormPos.x = (int)(MyTargetsGenTo[i].transform.position.x / MyTrueChunkSize);
            tgtNormPos.y = (int)(MyTargetsGenTo[i].transform.position.z / MyTrueChunkSize);


            int normGenBoundSize = MyGenBoundSize / MyTrueChunkSize;
            for (int x = 0; x < normGenBoundSize; x++) {
                for (int y = 0; y < normGenBoundSize; y++) {

                    Vector2Int normBoundPos = new Vector2Int(x + tgtNormPos.x - normGenBoundSize / 2, y + tgtNormPos.y - normGenBoundSize / 2);

                    if (!myHasChunkAtPos(normBoundPos.x, normBoundPos.y)) {
                        WorldCityChunk chunk = myCreateChunk();
                        chunk.MyGenerate(MyChunkSize, MyLinesCntPerChunkSide, MyChunkScale);
                        chunk.MyMapPos = new Vector2Int(normBoundPos.x, normBoundPos.y);
                        chunk.transform.position = new Vector3(normBoundPos.x * MyTrueChunkSize, 0, normBoundPos.y * MyTrueChunkSize);
                        chunks.Add(chunk);


                    }
                }
            }
        }

    }

    private void OnDrawGizmos() {
        for (int i = 0; i < MyTargetsGenTo.Count; i++) {
            Gizmos.DrawWireCube(MyTargetsGenTo[i].transform.position, new Vector3(MyGenBoundSize, 0, MyGenBoundSize));
        }
    }
    #endregion
}
