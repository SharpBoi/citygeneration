﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapBrokenLine : IRasterizeable {

    #region FIELDS


    private Vector2 startPos;
    private List<MapLine> lines = new List<MapLine>();
    private List<MapPoint> raster = new List<MapPoint>();
    #endregion

    #region PROPS
    public float Length { get; private set; }
    public List<MapPoint> Raster { get { return raster; } }
    public List<MapLine> Lines { get { return lines; } }
    #endregion

    #region FUNCS
    public void MoveTo(Vector2 pos) {
        startPos = pos;
    }
    public MapPoint GenTo(Vector2 dir, float len) {
        MapLine line;

        if (lines.Count == 0)
            line = new MapLine(startPos, startPos + dir * len);
        else
            line = new MapLine(lines[lines.Count - 1].B.Pos, lines[lines.Count - 1].B.Pos + dir * len);

        lines.Add(line);
        Length += len;

        return line.B;
    }
    public void Cut(MapLine fromLine, bool includeFromLine) {

        for(int i = lines.Count - 1; i >= 0; i--) {
            if (lines[i] == fromLine) {
                if (includeFromLine)
                    lines.Remove(fromLine);
                break;
            }

            lines.RemoveAt(lines.Count - 1);
        }

    }

    public void Rasterize(float gridSize) {
        raster.Clear();
        for (int i = 0; i < lines.Count; i++) {
            lines[i].Rasterize(gridSize);
            raster.AddRange(lines[i].Raster);
        }
    }
    #endregion

    #region UNITY FUNCS
    /// <summary>
    /// Рисует все свои линии
    /// </summary>
    public void GizmoDrawLine(Vector3 offset) {
        for (int i = 0; i < lines.Count; i++)
            lines[i].GizmoDrawLine(offset);
    }
    public void GizmoDrawRaster(Vector3 offset) {
        for (int i = 0; i < lines.Count; i++)
            lines[i].GizmoDrawRaster(offset);
    }
    #endregion
}
