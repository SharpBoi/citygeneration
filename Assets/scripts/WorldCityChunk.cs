﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldCityChunk : MonoBehaviour {

    #region FIELDS

    
    private MapChunk chunk;
    private List<WorldTile> tiles = new List<WorldTile>();
    #endregion

    #region PROPS
    //public MapChunk MyChunk { get { return chunk; } }
    public Vector2Int MyMapPos { get; set; }
    #endregion

    #region FUNCS
    public void MyGenerate(int chunkSize, int linesPerSide, int scale) {

        System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
        sw.Start();

        chunk = new MapChunk();
        chunk.Generate(chunkSize, linesPerSide);
        chunk.Scale(scale);
        chunk.Rasterize(1);

        sw.Stop();

        Debug.Log("Chunk gen time: " + sw.Elapsed.TotalSeconds + "sec; " + sw.ElapsedMilliseconds + "ms;");
    }

    public void MyAddTile(WorldTile tile, Vector3 worldPos) {
        tile.transform.SetParent(transform);
        tile.transform.position = worldPos;
    }
    #endregion

    #region UNITY FUNCS
    void Start () {
    }

    private void OnDrawGizmos() {
        if (chunk != null)
            chunk.GizmoDraw(transform.position);
    }
    #endregion

}
