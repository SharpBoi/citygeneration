﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapRect : IRasterizeable {

    #region FIELDS


    private Vector2 pos;
    private float width;
    private float height;

    private List<MapPoint> raster = new List<MapPoint>();
    private MapLine[] lines = new MapLine[4];
    #endregion

    #region PROPS
    public float Width {
        get { return width; }
        set {
            width = value;
            posLines();
        }
    }
    public float Height {
        get { return height; }
        set {
            height = value;
            posLines();
        }
    }
    public Vector2 Pos {
        get { return pos; }
        set {
            pos = value;
            posLines();
        }
    }

    public float Left { get { return Mathf.Min(pos.x, pos.x + width); } }
    public float Right { get { return Mathf.Max(pos.x, pos.x + width); } }
    public float Top { get { return Mathf.Max(pos.y, pos.y + height); } }
    public float Bottom { get { return Mathf.Min(pos.y, pos.y + height); } }

    public Vector2 Center { get { return pos + new Vector2(width, height) / 2; } }
    public float Area { get { return width * height; } }

    public List<MapPoint> Raster { get { return raster; } }
    public MapLine[] Lines { get { return lines; } }
    #endregion

    #region FUNCS
    public MapRect(float x, float y, float w, float h) {
        pos.x = x;
        pos.y = y;
        width = w;
        height = h;

        for (int i = 0; i < lines.Length; i++)
            lines[i] = new MapLine();

        posLines();
    }

    public MapRect IntersectionRect(MapRect other) {

        float l = Mathf.Max(Left, other.Left);
        float r = Mathf.Min(Right, other.Right);
        float t = Mathf.Min(Top, other.Top);
        float b = Mathf.Max(Bottom, other.Bottom);

        return new MapRect(l, b, r - l, t - b);
    }
    public bool Intersect(MapRect other) {
        float l = Mathf.Max(Left, other.Left);
        float r = Mathf.Min(Right, other.Right);
        float t = Mathf.Min(Top, other.Top);
        float b = Mathf.Max(Bottom, other.Bottom);

        return (r - l) * (t - b) > 0;
    }

    public void Rasterize(float gridSize) {
        raster.Clear();

        for (int i = 0; i < lines.Length; i++) {
            lines[i].Rasterize(gridSize);
            raster.AddRange(lines[i].Raster);
        }

        for (int i = 0; i < raster.Count; i++) {
            for (int j = i + 1; j < raster.Count; j++) {
                if (raster[i].Pos == raster[j].Pos)
                    raster.RemoveAt(j);
            }
        }
    }

    private void posLines() {
        lines[0].A.Pos = new Vector2(Left, Bottom);
        lines[0].B.Pos = new Vector2(Left, Top);

        lines[1].A.Pos = new Vector2(Left, Top);
        lines[1].B.Pos = new Vector2(Right, Top);

        lines[2].A.Pos = new Vector2(Right, Top);
        lines[2].B.Pos = new Vector2(Right, Bottom);

        lines[3].A.Pos = new Vector2(Right, Bottom);
        lines[3].B.Pos = new Vector2(Left, Bottom);
    }

    public void Draw() {
        for (int i = 0; i < lines.Length; i++) {
            //Gizmos.DrawLine(lines[i].A.Pos, lines[i].B.Pos);
            Gizmos.DrawLine(new Vector3(lines[i].A.Pos.x, 0, lines[i].A.Pos.y),
                new Vector3(lines[i].B.Pos.x, 0, lines[i].B.Pos.y));
        }
    }
    #endregion
}
