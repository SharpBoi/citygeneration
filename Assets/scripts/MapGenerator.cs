﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class MapGenerator : MonoBehaviour {

    #region FIELDS
    public float GridSize = 1;
    public int MaxLineLen = 4;
    public int MinLineLen = 2;
    public Transform TargetGenTo;

    private Map2D<MapRect> map;

    private float gizmoSize = 0.1f;
    #endregion

    #region UNITY FUNCS
    void Start() {
        map = new Map2D<MapRect>(GridSize);

        Stopwatch sw = new Stopwatch();
        sw.Start();
        for (int i = 0; i < 20; i++) {

            while (true) {
                Vector2 origin = Vector2.zero;
                if (map.RasterNodes.Count > 0)
                    origin = map.RasterNodes[Random.Range(0, map.RasterNodes.Count)].Pos;

                Vector2 size = new Vector2(
                    Random.Range(MinLineLen, MaxLineLen) * Mathf.Sign(Random.Range(-1, 1)),
                    Random.Range(MinLineLen, MaxLineLen) * Mathf.Sign(Random.Range(-1, 1)));

                MapRect nextRect = new MapRect(origin.x, origin.y, size.x, size.y);

                bool badGen = false;
                for (int j = 0; j < map.Rastables.Count; j++) {
                    if (nextRect.Intersect(map.Rastables[j])) {
                        MapRect intsctRect = nextRect.IntersectionRect(map.Rastables[j]);
                        if (intsctRect.Width < MinLineLen || intsctRect.Height < MinLineLen) {
                            badGen = true;
                            break;
                        }
                    }
                }

                if (badGen)
                    continue;
                else {

                    map.AddRastableAndRasterizeIt(nextRect);
                    map.Rasterize();

                    break;
                }
            }
        }
        sw.Stop();

        Debug.Log("Gen time ms: " + ((float)sw.ElapsedMilliseconds / 1000));
    }

    private void FixedUpdate() {



    }

    private void OnDrawGizmos() {
        if (map != null) {
            // Draw Rasterized points
            Gizmos.color = Color.white;
            for (int i = 0; i < map.Raster.Count; i++) {
                if (map.Raster[i].IsNode) Gizmos.color = Color.red;
                else Gizmos.color = Color.white;

                Gizmos.DrawSphere(new Vector3(map.Raster[i].Pos.x, 0, map.Raster[i].Pos.y), gizmoSize / 2);
                //Gizmos.DrawSphere(map.Raster[i].Pos.x * transform.forward + map.Raster[i].Pos.y * transform.forward, gizmoSize / 2);
            }

            Gizmos.color = Color.white;
            for (int i = 0; i < map.Rastables.Count; i++) {
                map.Rastables[i].Draw();
            }

            // Draw lines
            //Gizmos.color = Color.white;
            //for (int i = 0; i < map.lines.Count; i++) {
            //    Gizmos.DrawLine(
            //        new Vector3(map.lines[i].A.Pos.x, 0, map.lines[i].A.Pos.y),
            //        new Vector3(map.lines[i].B.Pos.x, 0, map.lines[i].B.Pos.y));
            //}
        }
    }
    #endregion

    #region FUNCS



    #endregion
}
