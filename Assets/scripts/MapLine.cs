﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapLine : IRasterizeable {

    #region FIELDS
    public MapPoint A;
    public MapPoint B;


    private List<MapPoint> raster = new List<MapPoint>();
    private Vector3 gizmoA, gizmoB;
    #endregion

    #region PROPS
    public List<MapPoint> Raster { get { return raster; } }
    public Vector2 Dir { get { return (B.Pos - A.Pos).normalized; } }
    #endregion

    #region FUNCS
    public MapLine()
    {
        A = new MapPoint(this);
        A.IsNode = true;

        B = new MapPoint(this);
        B.IsNode = true;
    }
    public MapLine(Vector2 a, Vector2 b)
    {
        A = new MapPoint(this) { Pos = a, IsNode = true };

        B = new MapPoint(this) { Pos = b, IsNode = true };
    }

    public void Rasterize(float gridSize)
    {
        raster.Clear();
        raster.Add(A);

        float dist = Vector2.Distance(A.Pos, B.Pos);
        Vector2 dir = (B.Pos - A.Pos).normalized;

        for (float i = gridSize; i < dist; i += gridSize)
        {
            raster.Add(new MapPoint(this)
            {
                Pos = A.Pos + dir * i
            });
        }

        raster.Add(B);
    }

    public override string ToString() {
        return A + "->" + B;
    }
    #endregion

    #region UNITY FUNCS
    /// <summary>
    /// Рисует свою линию и растеризованые точки
    /// </summary>
    public void GizmoDrawLine(Vector3 offset) {
        gizmoA.x = A.Pos.x;
        gizmoA.y = 0;
        gizmoA.z = A.Pos.y;

        gizmoB.x = B.Pos.x;
        gizmoB.y = 0;
        gizmoB.z = B.Pos.y;

        Gizmos.DrawLine(gizmoA + offset, gizmoB + offset);
    }
    public void GizmoDrawRaster(Vector3 offset) {
        Gizmos.color = Color.white;

        gizmoA.x = A.Pos.x;
        gizmoA.y = 0;
        gizmoA.z = A.Pos.y;

        gizmoB.x = B.Pos.x;
        gizmoB.y = 0;
        gizmoB.z = B.Pos.y;

        for (int i = 0; i < raster.Count; i++)
            raster[i].GizmoDraw(offset);
    }
    #endregion
}
