﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldTile : MonoBehaviour {

    #region FIELDS
    public float Size = 1;
    public float Height = 2;


    private Vector3 gizmoCenter, gizmoSize;
    private MeshRenderer[] rends;
    #endregion

    #region FUNCS
    public void MyShow() {
        for (int i = 0; i < rends.Length; i++)
            rends[i].enabled = true;
    }
    public void MyHide() {
        for (int i = 0; i < rends.Length; i++)
            rends[i].enabled = false;
    }
    #endregion

    #region UNITY FUNCS
    private void Start() {
        rends = GetComponentsInChildren<MeshRenderer>();
    }

    private void OnDrawGizmos() {
        gizmoCenter.x = transform.position.x;
        gizmoCenter.y = transform.position.y + Height / 2;
        gizmoCenter.z = transform.position.z;

        gizmoSize.x = Size;
        gizmoSize.y = Height;
        gizmoSize.z = Size;

        Gizmos.DrawWireCube(gizmoCenter, gizmoSize);
    }
    #endregion
}

enum MapTileType { Road, Building, Ground }