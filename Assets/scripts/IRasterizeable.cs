﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRasterizeable {
    void Rasterize(float gridSize);
    List<MapPoint> Raster { get; }
}
