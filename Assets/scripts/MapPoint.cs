﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapPoint {

    #region STATIC
    public static List<MapPoint> AllPoints = new List<MapPoint>();
    #endregion

    #region FIELDS
    public bool IsNode = false;


    private Vector2 pos;
    private MapLine parentLine;
    private Vector3 gizmoPnt;
    #endregion

    #region PROPS
    public MapLine ParentLine { get { return parentLine; } }
    public Vector2 Pos { get { return pos; } set { pos = value; } } 
    #endregion

    #region FUNCS
    public MapPoint(MapLine parent)
    {
        parentLine = parent;

        if (AllPoints.Contains(this) == false)
            AllPoints.Add(this);
    }

    public void SetPos(Vector2 pos)
    {
        this.pos = pos;
    }
    public void SetIsNode(bool value) {
        IsNode = value;
    }


    public override string ToString() {
        return pos.ToString();
    }
    #endregion

    #region UNITY FUNCS
    /// <summary>
    /// Рисует свою точку
    /// </summary>
    public void GizmoDraw(Vector3 offset) {
        if (IsNode)
            Gizmos.color = Color.red;
        else
            Gizmos.color = Color.white;

        gizmoPnt.x = pos.x;
        gizmoPnt.y = 0;
        gizmoPnt.z = pos.y;
        Gizmos.DrawSphere(gizmoPnt + offset, 0.1f);
    }
    #endregion
}
