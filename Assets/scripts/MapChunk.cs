﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapChunk : IRasterizeable{

    #region FIELDS


    private int chunkSize, linesPerSide;
    private List<MapLine> borderLines = new List<MapLine>();
    private List<MapPoint> borderRaster = new List<MapPoint>();
    
    private List<MapBrokenLine> insideSnakes = new List<MapBrokenLine>();
    private List<MapPoint> insideRaster = new List<MapPoint>();

    private List<MapPoint> raster = new List<MapPoint>();
    #endregion

    #region PROPS
    public int ChunkSize { get { return chunkSize; } }
    public int LinesPerSide { get { return linesPerSide; } }
    public List<MapPoint> Raster { get { return raster; } }
    #endregion

    #region FUNCS
    public void Scale(int scale) {

        chunkSize *= scale;

        for (int i = 0; i < insideRaster.Count; i++)
            insideRaster[i].Pos *= scale;

        for (int i = 0; i < borderRaster.Count; i++)
            borderRaster[i].Pos *= scale;
    }

    public void Generate(int chunkSize, int linesPerSide) {

        this.chunkSize = chunkSize;
        this.linesPerSide = linesPerSide;

        genBorderLines(1);
        rasterizeBorder(1);

        genInsideLines();
        rasterizeInside(1);
        //removeOverlapingNodes();
        //analyzeNodes();
    }
    private void genBorderLines(int gridSize) {
        MapLine a = new MapLine(Vector2.zero, new Vector2(chunkSize, 0));
        MapLine b = new MapLine(new Vector2(chunkSize, 0), new Vector2(chunkSize, chunkSize));
        MapLine c = new MapLine(new Vector2(chunkSize, chunkSize), new Vector2(0, chunkSize));
        MapLine d = new MapLine(new Vector2(0, chunkSize), Vector2.zero);

        borderLines.Clear();
        borderLines.Add(a);
        borderLines.Add(b);
        borderLines.Add(c);
        borderLines.Add(d);
    }
    private void genInsideLines() {

        Vector2 origin = Vector2.zero;
        Vector2 borderDir = Vector2.zero;
        Vector2 borderNormal;
        Vector2 pntOnBorder = Vector2.zero;
        List<Vector2> prevPntsOnBorder = new List<Vector2>();

        Vector2 snakeDir = Vector2.zero;
        int snakeSegmLen = 0;
        
        for (int deg = 0; deg <= 270; deg += 90) {

            // calc dir along border side of chunk
            borderDir = deg2Dir(deg);
            borderNormal = deg2Dir(deg + 90);

            // calc origin of corner of border
            if (deg == 0) origin = Vector2.zero;
            else if (deg == /*-*/90) origin = new Vector2(chunkSize, 0);
            else if (deg == /*-*/180) origin = new Vector2(chunkSize, chunkSize);
            else if (deg == /*-*/270) origin = new Vector2(0, chunkSize);

            // generate snake from each side of border
            prevPntsOnBorder.Clear();
            for (int i = 0; i < linesPerSide; i++) {

                while (true) {
                    pntOnBorder = origin + borderDir * Random.Range(1, chunkSize - 1);
                    if (prevPntsOnBorder.Contains(pntOnBorder) == false)
                        break;
                }

                // create snake and place it on border side
                MapBrokenLine snake = new MapBrokenLine();
                snake.MoveTo(pntOnBorder);
                
                // grow snake
                while (true) {

                    // calculate snake gen angle
                    while (true) {
                        snakeDir = rndDir(90);

                        if (snake.Lines.Count == 0) {
                            if (snakeDir == borderNormal)
                                break;
                        }
                        else if (snakeDir != -borderNormal)
                            break;
                    }
                    snakeSegmLen = 1;

                    // grow snake on a step forward
                    snake.GenTo(snakeDir, snakeSegmLen);
                    bool stopGenSnake = false;

                    for (int j = 0; j < borderRaster.Count; j++) {
                        if(snake.Lines[snake.Lines.Count -1 ].B.Pos == borderRaster[j].Pos) {
                            stopGenSnake = true;
                            break;
                        }
                    }

                    // stop gen snake
                    if (stopGenSnake) {
                        snake.Rasterize(1);
                        break;
                    }
                }
                insideSnakes.Add(snake);
            }

        }
    }
    private void removeOverlapingNodes() {

        Debug.Log("begin raster len: " + insideRaster.Count);
        int beginCnt = 0;
        while (beginCnt != insideRaster.Count) {
            beginCnt = insideRaster.Count;

            for (int i = 0; i < insideRaster.Count; i++) {
                insideRaster[i].IsNode = false;
                for (int j = i + 1; j < insideRaster.Count; j++) {
                    if (insideRaster[i].Pos == insideRaster[j].Pos) {

                        insideRaster.RemoveAt(j);
                        insideRaster[i].IsNode = true;

                        //Debug.Log("node ");
                    }
                }
            }
        }

        Debug.Log("afeter calc raster len: " + insideRaster.Count);
    }
    private void analyzeNodes() {
        for(int i = 0; i < insideRaster.Count; i++) {

        }
    }

    private float rndDeg(float step) {
        return Random.Range(0, (int)Mathf.Round(360f / step)) * step;
    }
    private Vector2 rndDir(float degStep) {
        float deg = rndDeg(degStep);
        return new Vector2(
            Mathf.Cos(deg * Mathf.Deg2Rad),
            Mathf.Sin(deg * Mathf.Deg2Rad));
    }
    private Vector2 deg2Dir(float deg) {
        return new Vector2(
            Mathf.Cos(deg * Mathf.Deg2Rad),
            Mathf.Sin(deg * Mathf.Deg2Rad));
    }

    private void rasterizeBorder(float gridSize) {
        borderRaster.Clear();
        for (int i = 0; i < borderLines.Count; i++) {
            borderLines[i].Rasterize(gridSize);
            borderRaster.AddRange(borderLines[i].Raster);
        }
    }
    private void rasterizeInside(float gridSize) {
        insideRaster.Clear();
        for (int i = 0; i < insideSnakes.Count; i++) {
            insideSnakes[i].Rasterize(gridSize);
            insideRaster.AddRange(insideSnakes[i].Raster);
        }
    }
    public void Rasterize(float gridSize) {
        raster.Clear();

        rasterizeBorder(gridSize);
        rasterizeInside(gridSize);

        raster.AddRange(borderRaster);
        raster.AddRange(insideRaster);
    }
    #endregion

    #region UNITY FUNCS
    /// <summary>
    /// Рисует границы чанка и внутренние ломаные линии
    /// </summary>
    public void GizmoDraw(Vector3 offset) {
        for (int i = 0; i < borderLines.Count; i++)
            borderLines[i].GizmoDrawLine(offset);

        for (int i = 0; i < borderRaster.Count; i++)
            borderRaster[i].GizmoDraw(offset);


        for (int i = 0; i < insideSnakes.Count; i++)
            insideSnakes[i].GizmoDrawLine(offset);

        for (int i = 0; i < insideRaster.Count; i++)
            insideRaster[i].GizmoDraw(offset);
    }
    #endregion
}
