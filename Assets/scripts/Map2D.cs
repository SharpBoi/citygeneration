﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map2D<T> where T : IRasterizeable {
    #region FIELDS


    private List<T> rastables = new List<T>();
    private List<MapPoint> rasterNodes = new List<MapPoint>();
    private List<MapPoint> raster = new List<MapPoint>();
    private float gridSize = 1;
    #endregion

    #region PROPS
    public List<MapPoint> Raster { get { return raster; } }
    public List<MapPoint> RasterNodes { get { return rasterNodes; } }
    public List<T> Rastables { get { return rastables; } }
    #endregion

    #region FUNCS
    public Map2D(float gridSize) {
        this.gridSize = gridSize;
    }

    public void AddRastableAndRasterizeIt(T rastable) {
        if (rastables.Contains(rastable) == false) {
            rastable.Rasterize(gridSize);
            rastables.Add(rastable);
        }
    }

    public void Rasterize() {
        raster.Clear();
        rasterNodes.Clear();

        for (int i = 0; i < rastables.Count; i++)
            raster.AddRange(rastables[i].Raster);

        for (int i = 0; i < raster.Count; i++) {
            for (int j = i + 1; j < raster.Count; j++) {
                if (raster[i].Pos == raster[j].Pos)
                    raster.RemoveAt(j);
            }

            if (raster[i].IsNode)
                rasterNodes.Add(raster[i]);
        }
    }
    public void ForceRasterizeAll() {
        for (int i = 0; i < rastables.Count; i++)
            rastables[i].Rasterize(gridSize);

        Rasterize();
    }
    public void ClearAllRaster() {
        raster.Clear();
        rasterNodes.Clear();
    }

    public bool HasPointAt(Vector2 pos, out MapPoint point) {
        for (int i = 0; i < raster.Count; i++) {
            if (raster[i].Pos == pos) {
                point = raster[i];
                return true;
            }
        }

        point = null;
        return false;
    }
    public bool HasPointAt(Vector2 pos) {
        MapPoint p;
        return HasPointAt(pos, out p);
    }
    public bool HasPointAlong(Vector2 origin, Vector2 dir, int checkCellsCount, out MapPoint point) {
        dir.Normalize();
        for (float i = gridSize; i < checkCellsCount * gridSize; i += gridSize) {
            if (HasPointAt(origin + dir * i, out point))
                return true;
        }

        point = null;
        return false;
    }
    public bool HasPointAlong(Vector2 origin, Vector2 dir, int checkCellsCount) {
        MapPoint p;
        return HasPointAlong(origin, dir, checkCellsCount, out p);
    }

    #endregion
}
